<?php
/*  a program for online registration
 *  Copyright (C) 2007-2013  Isfahanlug
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

error_reporting(0);
include 'db.php';
include 'functions.php';

connect('dbhostname', 'dbusername', 'dbpassword', 'dbname');

if(isset($_GET['act'])){
	$id = (int)$_GET['id'];
	$code = (int)$_GET['code'];
	mysql_query("UPDATE `main` SET `verify` = '1' WHERE `id`=$id and `code`=$code ");
	header('Location: http://isfahanlug.org/doku.php?id=%D8%A7%D8%B2_%D9%87%D9%85%D8%B1%D8%A7%D9%87%DB%8C%D8%AA%D8%A7%D9%86_%D9%85%D8%AA%D8%B4%DA%A9%D8%B1%DB%8C%D9%85');
	die();
}

if(isset($_POST['submit'])){
  $name = filter($_POST['name']);
  $family = filter($_POST['family']);
  $email = filter($_POST['email']);
  $phone = filter($_POST['phone']);
  $selectdis = (int)filter($_POST['selectdis']);
  $disname = filter($_POST['disname']);
  if($selectdis!=2) $disname='خیر';
  $work = filter($_POST['work']);
  $server = filter(serialize($_SERVER));
  $code = rand(10000,99999);
  $pmail = mysql_fetch_row(mysql_query("SELECT `email` from `main` where `email`='$email'"));
  $pmail = $pmail[0];
  if($pmail==$email){
    $msg = 'شما قبلا ثبت نام کرده اید.';
  }
  else {
    mysql_query("INSERT INTO `main` (`name`, `family`, `email`, `phone`, `edu`, `work`, `date`, `server`, `code`)
                VALUES ('$name','$family','$email','$phone','$disname','$work',  NOW(), '$server','$code')");
    $lastid = mysql_insert_id();
    sendmail($code,$email,$lastid);
    $msg = 'با تشکر از ثبت نام شما<br />
  ایمیلی حاوی کد فعال سازی برای شما ارسال شده است. <br />
  برای تکمیل ثبت نام بر روی لینک آن کلیک کنید.';
  }
}
$total = mysql_fetch_row(mysql_query("SELECT count(`id`) from `main` where `verify` = '1'"));
$total = num2fa((int)$total[0]);
$users = mysql_query("SELECT `name`, `family` from `main` where verify=1 order by `id` desc limit 12");
$users2 = mysql_query("SELECT `name`, `family` from `main` where verify=1 order by `id` asc");
?>

<!DOCTYPE html>
<html lang="fa">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        روز آزادی نرم‌افزار!
    </title>
    <script type="text/javascript" src="js/v.js">
    </script>
    <style type="text/css">
      body, input, select, div, a{
        font-family: Tahoma;
        font-size: 12px;
        direction: rtl;
        text-decoration: none;
      }
      input{
        border: 1px solid #336633;
        color: #111166;
        padding: 1px;
        margin: 2px;
      }
      fieldset{
        padding: 5px;
      }
      .crct {
        display:none;
        float: left}
      .titl {
        float: right;
      }
      .inpt {
        float: left;
        width: 60%;
      }
      .cls{
        clear: both;
      }
      ul{
        list-style-image: url(images/arw.gif);
        padding:2px;
        margin: 0 15px 0 0;
      }
      li{
        padding: 0;
        margin: 0;
      }
      .uall li{
        width:118px;
        overflow: hidden;
        float: right;
        white-space: nowrap;
      }
      .uall{
        list-style: url(images/arw.gif) inside;
        margin: 0px}
    </style>
  </head>
  <body>
    <div style="margin: auto; border: 1px soldi #0; width: 500px;" id="reg">
      <fieldset style="width: 300px;float: right;">
        <legend class="sectiontableheader">
          مشخصات :
        </legend>
        <?php if(isset($msg)) {echo '
<p>
'.$msg.'
</p>
';} ?>
  <form id="mainform" action="?" method="post" onsubmit="return val()">
    <div id="name_div">
      <div class="titl">
        نام :
      </div>
      <div class="inpt">
        <input type="text" name='name' size="15" onkeyup="myform.validate()">
      </div>
      <div class="crct">
        <img src="images/corr.gif" />
      </div>
      <div class="crct">
        <img src="images/err.gif" />
      </div>
      <br class="cls" />
    </div>
    <div id="family_div">
      <div class="titl">
        نام خانوادگی :
      </div>
      <div class="inpt">
        <input type="text" name='family' size="15" onkeyup="myform.validate()">
      </div>
      <div class="crct">
        <img src="images/corr.gif" />
      </div>
      <div class="crct">
        <img src="images/err.gif" />
      </div>
      <br class="cls" />
    </div>
    <div id="email_div">
      <div class="titl">
        ایمیل :
      </div>
      <div class="inpt">
        <input type="text" name='email' dir="ltr" size="15" onkeyup="myform.validate()">
      </div>
      <div class="crct">
        <img src="images/corr.gif" />
      </div>
      <div class="crct">
        <img src="images/err.gif" />
      </div>
      <br class="cls" />
    </div>
    <div>
      <div class="titl">
        تلفن تماس :
      </div>
      <div class="inpt">
        <input type="text" name='phone' dir="ltr" size="15">
      </div>
      <br class="cls" />
    </div>
    <div>
      <div class="titl">
        زمینه کاری :
      </div>
      <div class="inpt">
        <input type="text" name='work' size="15">
      </div>
      <br class="cls" />
    </div>
    <div>
      <div class="titl">
        کامپیوتر شخصی خود را همراه می آورید؟ :
      </div>
      <div class="inpt" style="width:25%">
        <select id='selectdis' name='selectdis' onchange="dist()">
          <option value="0">
            ---
          </option>
          <option value="1">
            خیر
          </option>
          <option value="2">
            بلی
          </option>
        </select>
      </div>
      <br class="cls" />
    </div>
    <div id='distro' style="display:none">
      <div class="titl">
        نام توزیع :
      </div>
      <div class="inpt">
        <input type="text" name='disname' size="15" >
      </div>
      <br class="cls" />
    </div>
    <input type="submit" name="submit" value="ثبت نام" style="display: block; margin:5px auto;" />
  </form>
  <br />
  <br />
  <div style="margin: 50px auto 50px auto; width: 50px; display: none" id="loading">
    <img src="images/load.gif" />
  </div>
  </fieldset>
  <fieldset style="width: 150px;float: left">
    <legend class="sectiontableheader">
      همراهان :
    </legend>
    <ul>
      <?php
while ($user=mysql_fetch_row($users) ){
echo "
<li>
{$user[0]} {$user[1]}
</li>
";
}
?>
  </ul>
  در کل
  <?=$total?>
  نفر
  <a href="#" onclick="$('reg').style.display='none';$('all').style.display='block'" >
    تمامی افراد
  </a>
  </fieldset>
  <br class="cls"/>
  </div>
  <div style="margin: auto; border: 1px soldi #0; width: 500px; display: none" id="all">
    <fieldset style="width: 480px;display: block; margin: auto;">
      <legend class="sectiontableheader">
        همراهان :
      </legend>
      <ul class='uall'>
        <?php
while ($user=mysql_fetch_row($users2) ){
echo "
<li>
{$user[0]} {$user[1]}
</li>
";
}
?>
  </ul>
  <br class="cls"/>
  در کل
  <?=$total?>
  نفر
  <a href="#" onclick="$('all').style.display='none';$('reg').style.display='block'" >
    برگشت
  </a>
  </fieldset>
  </div>
  </body>
</html>
