CREATE TABLE IF NOT EXISTS `main` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(64) NOT NULL default '',
  `family` varchar(64) NOT NULL default '',
  `email` text NOT NULL,
  `phone` varchar(64) NOT NULL default '',
  `edu` text NOT NULL,
  `work` text NOT NULL,
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `verify` tinyint(1) NOT NULL default '0',
  `code` varchar(8) NOT NULL default '',
  `server` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;