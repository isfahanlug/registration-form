<?php
/*  a program for online registration
 *  Copyright (C) 2007-2013  Isfahanlug
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
session_start();

error_reporting(0);
include 'db.php';
include 'functions.php';

connect('dbhostname', 'dbusername', 'dbpassword', 'dbname');


if(isset($_POST['school'])){
	$_SESSION['school'] = $_POST['school'];
}

if(isset($_GET['csv']) and $_SESSION['school']=="salman"){
	header('Content-type: text/plain');
	header('Content-Disposition: attachment; filename="lug.csv"');
	echo '"id", "name", "family", "email", "phone" , "edu" , "work" , "date" , "verify"
';
	$result = mysql_query("SELECT `id` , `name` , `family` , `email` , `phone` , `edu` , `work` , `date` , `verify` FROM `main` order by `id` ");
	while($row = mysql_fetch_row($result)){
		echo $row[0].',"'.$row[1].'","'.$row[2].'","'.$row[3].'","'.$row[4].'","'.$row[5].'","'.$row[6].'","'.$row[7].'","'.$row[8].'"
';
	}
	die();
}

$total = mysql_fetch_row(mysql_query("SELECT count(`id`) from `main`"));
$total = num2fa((int)$total[0]);
$active = mysql_fetch_row(mysql_query("SELECT count(`id`) from `main` where `verify`=1"));
$active = num2fa((int)$active[0]);
$users = mysql_query("SELECT `name`, `family` from `main` where `verify`=1 order by `id` desc limit 0,100");
$users2 = mysql_query("SELECT `name`, `family` from `main` where `verify`=0 order by `id` desc limit 0,100");
?>

<!DOCTYPE html>
<html lang="fa">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        روز آزادی نرم‌افزار
    </title>
    <style type="text/css">
      body, input, div{
        font-family: Tahoma;
        font-size: 12px;
        direction: rtl;
      }
      input{
        border: 1px solid #336633;
        color: #111166;
        padding: 1px;
        margin: 2px;
      }
      .crct {
        display:none;
        float: left}
      .titl {
        float: right;
      }
      .inpt {
        float: left;
        width: 60%;
      }
      .cls{
        clear: both;
      }
      ul{
        list-style-image: url(arw.gif);
        padding:2px;
        margin: 0 15px 0 0;
      }
      li{
        padding: 0;
        margin: 0;
      }
    </style>
  </head>
  <body>
    <div style="margin: auto; border: 1px soldi #0; width: 400px;">
      <?php if($_SESSION['school']=="salman"){ ?>
      <a href="?csv=1" >
        دانلود به فرمت CSV
      </a>
      <br />
      <fieldset style="width: 150px;float: left">
        <legend class="sectiontableheader">
          فعال :
        </legend>
        <ul>
          <?php
while ($user=mysql_fetch_row($users) ){
echo "
<li>
{$user[0]} {$user[1]}
</li>
";
}
?>
  </ul>
  فعال
  <?=$active?>
  نفر

  </fieldset>

  <fieldset style="width: 150px;float: right">
    <legend class="sectiontableheader">
      غیر فعال :
    </legend>
    <ul>
      <?php
while ($user=mysql_fetch_row($users2) ){
echo "
<li>
{$user[0]} {$user[1]}
</li>
";
}
?>
  </ul>
  در کل
  <?=$total?>
  نفر
  </fieldset>
  <br style="cls"/>
  <?php }
else { ?>
  <form action="?" method="post" >
    نام دبستان من؟
    <input type="text" name="school" />

    <input type="submit" value="ارسال" />
  </form>
  <?php } ?>
  </div>
  </body>
</html>
