<?php
/*  a program for online registration
 *  Copyright (C) 2007-2013  Isfahanlug
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function connect($db_hostname, $db_user, $db_pass, $db_name){
  @mysql_connect($db_hostname, $db_user, $db_pass);
  if(!@mysql_select_db($db_name)){
    echo 'err:db'; die();
  }
  mysql_query("SET NAMES 'utf8'");
}
?>