<?php
/*  a program for online registration
 *  Copyright (C) 2007-2013  Isfahanlug
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function filter($str,$save=true){
  $search  = array("ي", "script");
  $replace = array("ی", "scirpt");
  $str = str_replace($search, $replace, $str);

  $str = trim($str);
  $str = strip_tags($str);

  if ($save == true) {
    $str = mysql_real_escape_string($str);
  } else {
    $str = stripslashes($str);
  }
  return $str;
}

function num2fa($str){
  $num = strval($str);
  $res = '';
  for ($i=0; $i<strlen($num); $i++) {
    if (ord($num{$i})>=0x30 && ord($num{$i})<0x3A) {
      $res .= code2utf(0x6F0/*0x660*/+$num{$i});
    } else $res .= $num{$i};
  }
  return $res;
}

function code2utf($num){
  if($num<128)return chr($num);
  if($num<2048)return chr(($num>>6)+192).chr(($num&63)+128);
  if($num<65536)return chr(($num>>12)+224).chr((($num>>6)&63)+128).chr(($num&63)+128);
  if($num<2097152)return chr(($num>>18)+240).chr((($num>>12)&63)+128).chr((($num>>6)&63)+128). chr(($num&63)+128);
  return '';
}

function sendmailagain($code, $email, $id){
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
  // Additional headers
  $headers .= "To: $email <$email>" . "\r\n";
  $headers .= 'From: isfahansfd.org <info@isfahansfd.org>' . "\r\n";
  $subject="Welcome to Software Freedom Day (SFD) 2013";

  $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title></title>
</head><body style="background-color: white;">
  <div style="border:none; margin: auto; padding: 50px; width: 500px; direction: rtl;font-family: Tahoma; font-size: 12px;">
    <div id="content">
      <div><center> کاربر گرامی به جشن ما خوش آمدید.</center><br/>
       شما هنوز ثبت‌نام خود را تکمیل نکرده‌اید، لطفا اگر از حضور خود در جشن اطمینان دارید، ثبت‌نام خود را در همایش روز آزادی نرم‌افزار ۲۰۱۳ تایید کنید.<br />
       لطفا جهت تکمیل ثبت نامتان <a href="http://2013.isfahansfd.org/private/signup/index.php?act=1&id='.$id.'&code='.$code.'">اینجا</a> را کلیک کنید. 
       <br /> <br />
   	و یا از لینک زیر استفاده کنید 
       <br />
        <br />
       <div style="float: left">http://2013.isfahansfd.org/private/signup/index.php?act=1&id='.$id.'&code='.$code.' 
       	<br /> 
        <br />
        </div>
<div style="clear:both">
شنبه ۳۰ شهریور ماه ۱۳۹۲ ساعت ۱۵:۳۰<br />

مکان: سالن اجتماعات کتابخانه ی مرکزی شهرداری اصفهان<br />
آدرس : اصفهان - خیابان باغ گلدسته - کتابخانه ی مرکزی شهرداری اصفهان
<br /><br />
</div>
       <div><b> باتشکر، گروه کاربران لینوکس اصفهان </b></div>
       <br />
       
    </div>
  </div>
</body></html>';
       

  $mail_result=mail($email,$subject,$mail_content,$headers);
}

function sendmail($code, $email, $id){
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
  // Additional headers
  $headers .= "To: $email <$email>" . "\r\n";
  $headers .= 'From: isfahansfd.org <info@isfahansfd.org>' . "\r\n";
  $subject="Welcom to Software Freedom Day (SFD) 2013";

  $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/><title></title>
</head><body style="background-color: white;">
  <div style="border:none; margin: auto; padding: 50px; width: 500px; direction: rtl;font-family: Tahoma; font-size: 12px;">
    <div id="content">
      <div><center> کاربر گرامی به جشن ما خوش آمدید.</center><br/>
       لطفا جهت تکمیل ثبت نامتان <a href="http://2013.isfahansfd.org/private/signup/index.php?act=1&id='.$id.'&code='.$code.'">اینجا</a> را کلیک کنید. 
       <br /> <br />
   	و یا از لینک زیر استفاده کنید 
       <br />
        <br />
       <div style="float: left">http://2013.isfahansfd.org/private/signup/index.php?act=1&id='.$id.'&code='.$code.' 
       	<br /> 
        <br />
        </div>
<div style="clear:both">
شنبه ۳۰ شهریور ماه ۱۳۹۲ ساعت ۱۵:۳۰<br />

مکان: سالن اجتماعات کتابخانه ی مرکزی شهرداری اصفهان<br />
آدرس : اصفهان - خیابان باغ گلدسته - کتابخانه ی مرکزی شهرداری اصفهان
<br /><br />
</div>
       <div><b> باتشکر، گروه کاربران لینوکس اصفهان </b></div>
       <br />
       
    </div>
  </div>
</body></html>';
       

  $mail_result=mail($email,$subject,$mail_content,$headers);
}
